<?php

error_reporting(E_ERROR);

// Constants
define('maxLevel', 25);
define('totalQuests', 4933);
define('npcList', [0,
    1 => 'Edabit', 2 => 'Exercism',
    3 => 'HackerRank', 4 => 'URI Online Judge',
    5 => 'Project Euler', 6 => 'GeeksforGeeks']);

// Database
$db = new SQLite3("db/data.db");
$usersSQL = $db->query('SELECT * FROM users');

// Player variables
$player = $usersSQL->fetchArray(SQLITE3_ASSOC);
$xpMax = xpMax($player['level']);

$usersSQL->finalize();

// Functions
function xpMax($i) { return ($i * 5) + ~~($i * 10 * ($i ** 2)); }
function checkLevel() {
    global $db, $player;
    while($player['xp'] >= xpMax($player['level'])) {
        $db->exec('UPDATE users SET level = (level + 1) WHERE id = 1');
        echo "\e[32m\e[1m [!]\e[0m Parabéns! Você subiu para o nível ".
        "\e[1m\e[33m". ($player['level'] + 1) ."\n";
        $player['level']++;
    }
}

function accountReset() {
    global $db;
    $db->exec('UPDATE users SET level = 1 WHERE id = 1');
    $db->exec('UPDATE users SET quests = 0 WHERE id = 1');
    $db->exec('UPDATE users SET xp = 0 WHERE id = 1');
    $db->exec('DELETE FROM quests');
    $db->exec('DELETE FROM sqlite_sequence WHERE name = \'quests\'');
    echo "\e[32m\e[1m [!]\e[0m Sua conta foi resetada com sucesso!\n";
}

function completeQuest($xpQuest, $rankQuest, $i) {
    global $db, $player;

    if(!$i) {
        echo "\e[31m\e[1m [!]\e[0m Você deve especificar o número do NPC\n";
        exit(1);
    }

    if($player['level'] < maxLevel) {
        $db->exec('UPDATE users SET quests = (quests + 1) WHERE id = 1');
        $stmt = $db->prepare("INSERT INTO quests (rank, npc) VALUES (?, ?)");
        $stmt->bindValue('1', $rankQuest, SQLITE3_TEXT);
        $stmt->bindValue('2', npcList[$i], SQLITE3_TEXT);
        $stmt->execute();

        $stmt = $db->prepare('UPDATE users SET xp = (xp + (?)) WHERE id = 1');
        $stmt->bindValue(1, $xpQuest, SQLITE3_INTEGER);
        $stmt->execute();

        $player['xp'] += $xpQuest;
        checkLevel();
        echo "\e[32m\e[1m [!]\e[0m \e[1m(". npcList[$i] .")\e[0m Quest de"
        ." \e[33mrank \e[1m$rankQuest\e[0m concluída! "
        ."\e[33m(+$xpQuest de XP)\e[0m\n";

    } else {
        echo "\e[31m\e[1m [!]\e[0m Você já atingiu o nível máximo!\n";
        exit(1);
    }

    exit(0);
}

function showProfile() {
    global $player;
    echo "\n\e[1m\e[33m * \e[0m[ Personagem ]   \e[1m\e[37m". $player['name']
        ."\n\e[33m * \e[0m[  Espírito  ]   \e[1m\e[33m". $player['spirit']
        ."\n\e[33m * \e[0m[   Nível    ]   \e[1m\e[32m". $player['level']
        ." \e[0m(\e[1m". (xpMax($player['level']) - $player['xp'])
        ." XP\e[0m para o próximo nível)"
        ."\n\e[1m\e[33m * \e[0m[   Missões  ]   \e[1m"
        .(totalQuests - $player['quests'])
        ." \e[32m(". $player['quests']. " concluídas)\e[0m\n\n";
}

function showQuests() {
    global $db;

    $questsSQL = $db->query('SELECT * FROM quests');
    $ranksList = ['S' => 0, 'A' => 0, 'B' => 0, 'C' => 0];
    $i = 1;

    while($i) {
        $i = $questsSQL->fetchArray(SQLITE3_ASSOC);
        $ranksList[($i['rank'])] += 1;
    }

    $questsSQL->finalize();

    echo "\n\e[0m\e[33m  \e[0m[ \e[31mRank \e[1mS\e[0m ] \e[31m\e[1m"
    ."\e[0m\e[33m   \e[0m[ \e[33mRank \e[1mA\e[0m ] \e[33m\e[1m"
    ."\e[0m\e[33m   \e[0m[ \e[36mRank \e[1mB\e[0m ] \e[36m\e[1m"
    ."\e[0m\e[33m   \e[0m[ \e[32mRank \e[1mC\e[0m ] \e[32m\e[1m"
    . "\n\e[1m\e[31m       "
        .$ranksList['S'] ."             \e[33m"
        .$ranksList['A'] ."             \e[36m"
        .$ranksList['B'] ."             \e[32m"
        .$ranksList['C'] ."\n\n";
}

switch($argv[1]) {
    case "--quest-c": completeQuest(8, 'C', $argv[2]); break;
    case "--quest-b": completeQuest(16, 'B', $argv[2]); break;
    case "--quest-a": completeQuest(56, 'A', $argv[2]); break;
    case "--quest-s": completeQuest(72, 'S', $argv[2]); break;
    case "--quests": showQuests(); break;
    case "--confirm-reset": accountReset(); break;
    default: showProfile(); break;
}
