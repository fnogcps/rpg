## Tabela de Usuários
```sql
CREATE TABLE users (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    user TEXT NOT NULL UNIQUE,
    class TEXT NOT NULL,
    quests INTEGER NOT NULL,
    level INTEGER NOT NULL,
    xp INTEGER NOT NULL
);
```

## Tabela de Quests
```sql
CREATE TABLE quests (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    rank CHARACTER(1) NOT NULL,
    npc VARCHAR(24) NOT NULL
);
```

### Criação de personagem
```sql
INSERT INTO users (name, spirit, quests, level, xp) VALUES ("Felipe", "JavaScript", 0, 1, 0);
```

# Classificação de quests
  *  Rank C: exercícios fáceis ou muito fáceis
  *  Rank B: exercícios de nível médio (intermediários)
  *  Rank A: exercícios de nível difícil
  *  Rank S: exercícios de nível expert (muito difíceis)

# NPCs
  1. Edabit           (1300) **(apenas para JS)**
  2. Exercism         (103)  **(apenas para JS)**
  3. HackerRank       (565)
  4. URI Online Judge (2052)
  5. Project Euler    (701)
  6. GeeksforGeeks    (212)

Total: 4933 **(quests)**
